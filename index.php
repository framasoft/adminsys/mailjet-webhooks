<?php
    require "vendor/autoload.php";

    use Curl\Curl;

    openlog("mailjet-webhook", LOG_PID | LOG_PERROR, LOG_SYSLOG);

    $lm_url = getenv("LM_URL");
    $lm_user = getenv("LM_USER");
    $lm_password = getenv("LM_PASSWORD");
    $LM_LIST = 2;

    $data = file_get_contents("php://input");
    $json = json_decode($data, true);

    header('content-type: application/json; charset=utf-8');

    function get_id(string $email) {
        global $lm_url, $lm_user, $lm_password, $LM_LIST;

        $email = str_replace("'", "''", $email);
        $payload = [
            "page" => "1",
            "per_page" => "100",
            "query" => "subscribers.email = '$email'",
        ];
        $curl = new Curl();
        $curl->setBasicAuthentication($lm_user, $lm_password);
        $curl->setDefaultJsonDecoder($assoc = true);
        $curl->get("$lm_url/api/subscribers", $payload);
        if ($curl->error) {
            syslog(LOG_INFO, "Error " . $curl->errorCode . ": " . $curl->errorMessage);
            http_response_code(502);
            echo json_encode([
                "status" => "error",
                "msg" => "Error " . $curl->errorCode . " on /api/subscribers: " . $curl->errorMessage
            ]);
            return [ null, null, null ];
        } else {
            $result = $curl->response;
            if (sizeof($result["data"]["results"]) == 1) {
                foreach ($result["data"]["results"][0]["lists"] as $list) {
                    if ($list["id"] == $LM_LIST) {
                        if ($list["subscription_status"] != "unsubscribed") {
                            return [ $result["data"]["results"][0]["id"], $result["data"]["results"][0]["status"], true ];
                        }
                    }
                }
                return [ $result["data"]["results"][0]["id"], $result["data"]["results"][0]["status"], false ];
            } else if (sizeof($result["data"]["results"]) == 0) {
                syslog(LOG_INFO, "No result for $email");
                return [ null, null, null ];
            } else {
                syslog(LOG_INFO, "Too much results for $email: " . sizeof($result["data"]["results"]));
                return [ null, null, null ];
            }
        }
    }

    function block_user(string $email, string $event) {
        global $lm_url, $lm_user, $lm_password;

        syslog(LOG_INFO, "Blocking $email, reason: $event");
        [ $s_id, $s_status, $subscribed_to_nl ] = get_id($email);
        if (is_null($s_id)) {
            syslog(LOG_INFO, "No subscriber id: $email");
            return;
        } else if ($s_status == "blocklisted") {
            syslog(LOG_INFO, "Result for blocking $email: already blocklisted");
            return;
        } else {
            $curl = new Curl();
            $curl->setBasicAuthentication($lm_user, $lm_password);
            $curl->setDefaultJsonDecoder($assoc = true);
            $curl->put("$lm_url/api/subscribers/$s_id/blocklist");
            if ($curl->error) {
                syslog(LOG_INFO, "Error: " . $curl->errorMessage);
                http_response_code(502);
                echo json_encode([
                    "status" => "error",
                    "msg" => "Error " . $curl->errorCode . " on /api/subscribers/$s_id/blocklist: " . $curl->errorMessage
                ]);
                return;
            } else {
                $res = $curl->response["data"] ? 'ok' : 'not ok';
                syslog(LOG_INFO, "Result for blocking $email: $res");
                echo json_encode([
                    "status" => "success",
                    "msg" => "Result for blocking $email: $res"
                ]);
                return;
            }
        }
    }

    function unsub_user(string $email) {
        global $lm_url, $lm_user, $lm_password, $LM_LIST;

        syslog(LOG_INFO, "Unsubscribing $email");
        [ $s_id, $s_status, $subscribed_to_nl ] = get_id($email);
        if (is_null($s_id)) {
            syslog(LOG_INFO, "No subscriber id: $email");
            return;
        } else if ($subscribed_to_nl) {
            syslog(LOG_INFO, "Subscriber id: $s_id");
            $curl = new Curl();
            $curl->setBasicAuthentication($lm_user, $lm_password);
            $curl->setDefaultJsonDecoder($assoc = true);
            $payload = [
                "ids" => [$s_id],
                "action" => "unsubscribe",
                "target_list_ids" => [$LM_LIST]
            ];
            $curl->setHeader("Content-Type", "application/json");
            $curl->put("$lm_url/api/subscribers/lists", $payload);
            if ($curl->error) {
                syslog(LOG_INFO, "Error " . $curl->errorCode . ": " . $curl->errorMessage);
                http_response_code(502);
                echo json_encode([
                    "status" => "error",
                    "msg" => "Error " . $curl->errorCode . " on /api/subscribers/lists: " . $curl->errorMessage
                ]);
                return;
            } else {
                $res = $curl->response["data"] ? 'ok' : 'not ok';
                syslog(LOG_INFO, "Result for unsubscribing $email: $res");
                echo json_encode([
                    "status" => "success",
                    "msg" => "Result for unsubscribing $email: $res"
                ]);
                return;
            }
        } else {
            syslog(LOG_INFO, "Result for unsubscribing $email: not a subscriber");
            return;
        }
    }
    function process_event($json) {
        if (isset($json["email"]) && $json["email"] !== '') {
            switch ($json["event"]) {
                case "spam":
                    block_user($json["email"], $json["event"]);
                    break;
                case "blocked":
                    block_user($json["email"], $json["event"]);
                    break;
                case "bounce":
                    if ($json["hard_bounce"] || $json["blocked"]) {
                        block_user($json["email"], $json["event"]);
                    }
                    break;
                case "unsub":
                    unsub_user($json["email"]);
                    break;
            }
        } else {
            syslog(LOG_INFO, "No email in payload");
            http_response_code(200);
            echo json_encode([
                "status" => "error",
                "msg" => "No email in payload"
            ]);
            return;
        }
    }
    if (array_is_list($json)) {
        syslog(LOG_INFO, "Payload is an array");
        foreach ($json as $event) {
            process_event($event);
        }
    } else {
        process_event($json);
    }
?>
