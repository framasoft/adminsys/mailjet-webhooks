# Mailjet webhooks

Simple PHP app to automatically block or unsubscribe email addresses from [Listmonk](https://listmonk.app/) when notified through [Mailjet webhooks](https://dev.mailjet.com/email/guides/webhooks/).

## Installation

```bash
git clone https://framagit.org/framasoft/adminsys/mailjet-webhooks.git
cd mailjet-webhooks
composer install
```

Don’t forget to protect your app with a HTTP authentication.

## License

Licensed under the terms of the GNU Affero GPLv3. See [LICENSE](LICENSE) file.
